import {createApiRequest} from './config';
import {ApiEndpoints, HttpMethods} from './constants';
import type {UserDetails} from '~/state';
import {DataWithAccessToken} from './types';

export const ApiService = {
  doLogin: ({
    email,
    password,
    accessToken,
  }: DataWithAccessToken<{
    email: string;
    password: string;
  }>) =>
    createApiRequest<UserDetails>({
      endpoint: ApiEndpoints.User.Login,
      withAuth: true,
      accessToken,
      data: {
        email,
        password,
      },
      method: HttpMethods.Post,
    }),
};
