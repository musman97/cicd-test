import {AppEnv, AppEnvs} from '~/constants';

const STAGING_BASE_URL = 'www.expamle-staging.com/';
const PROD_BASE_URL = 'www.example-prod.com/';

export enum ApiErrorMessages {
  General = 'Something went wrong',
  Network = 'Network error',
  RequestAlreadyExists = 'Request already exists',
  UnableToSendRequest = 'Unable to send request',
  BadRequest = 'The data entered is invalid',
}

export enum HttpMethods {
  Get = 'Get',
  Post = 'Post',
  Put = 'Put',
  Patch = 'Patch',
  Delete = 'Delete',
}

export enum ApiStates {
  Idle = 'Idle',
  Pending = 'Pending',
  Success = 'Success',
  Failure = 'Failure',
}

export const ApiEndpoints = {
  User: {
    Login: 'auth/login',
  },
};

export const BaseUrl =
  AppEnv === AppEnvs.Staging ? STAGING_BASE_URL : PROD_BASE_URL;
