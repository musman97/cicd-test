import Axios, {AxiosRequestConfig, AxiosResponse} from 'axios';
import {ApiErrorMessages, HttpMethods} from './constants';
import {isError, isObjectNotEmpty} from '~/utils';
import {BaseUrl} from './constants';
import {
  ApiFailureResult,
  ApiRequestConfig,
  ApiResult,
  ApiSuccessResult,
  CustomErrorHandler,
  GeneralApiResponseData,
} from './types';

const axios = Axios.create({
  baseURL: BaseUrl,
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
});

const getAuthHeader = (accessToken: string) => ({
  Authorization: `Bearer ${accessToken}`,
});

function createRequestConfig<D>({
  withAuth,
  data,
  accessToken,
}: ApiRequestConfig<D>) {
  const requestConfig: AxiosRequestConfig<D> = {};

  if (withAuth) {
    requestConfig.headers = getAuthHeader(accessToken);
  }

  if (data && isObjectNotEmpty(data)) {
    requestConfig.data = data;
  }

  return requestConfig;
}

export const createApiSuccessResult = <D = undefined>(
  result?: Partial<ApiSuccessResult<D>>,
): ApiSuccessResult<D> => ({
  success: true,
  message: result?.message ?? 'OK',
  code: result?.code ?? 200,
  data: result?.data,
});

export const createApiFailureResult = <E>(
  result?: Partial<ApiFailureResult<E>>,
): ApiFailureResult<E> => ({
  success: false,
  message: result?.message ?? ApiErrorMessages.General,
  code: result?.code ?? -1,
  cause: result?.cause,
});

export function handlerError(error: unknown): ApiFailureResult {
  if (Axios.isAxiosError(error)) {
    const failureResult = createApiFailureResult({cause: error});
    const statusCode = error.response?.status;

    if (statusCode === 0) {
      failureResult.message = ApiErrorMessages.Network;
      failureResult.code = statusCode;
    } else if (statusCode) {
      const data = error.response?.data as GeneralApiResponseData;
      failureResult.message = data?.message || ApiErrorMessages.General;
      failureResult.code = statusCode;
    } else {
      failureResult.message = ApiErrorMessages.UnableToSendRequest;
      failureResult.code = -1;
    }

    return failureResult;
  }
  if (isError(error)) {
    return createApiFailureResult({
      message: ApiErrorMessages.UnableToSendRequest,
      code: -1,
      cause: error,
    });
  } else {
    return createApiFailureResult({
      message: ApiErrorMessages.UnableToSendRequest,
      code: -1,
    });
  }
}

export function doGet<R>(requestConfig: ApiRequestConfig) {
  const config = createRequestConfig(requestConfig);
  return axios.get<R>(requestConfig.endpoint, config);
}

export function doPost<R>(requestConfig: ApiRequestConfig) {
  const config = createRequestConfig(requestConfig);
  return axios.post<R>(requestConfig.endpoint, requestConfig.data, config);
}

export function doPut<R>(requestConfig: ApiRequestConfig) {
  const config = createRequestConfig(requestConfig);
  return axios.put<R>(requestConfig.endpoint, requestConfig.data, config);
}

export function doPatch<R>(requestConfig: ApiRequestConfig) {
  const config = createRequestConfig(requestConfig);
  return axios.patch<R>(requestConfig.endpoint, requestConfig.data, config);
}

export function doDelete<R>(requestConfig: ApiRequestConfig) {
  const config = createRequestConfig(requestConfig);
  return axios.delete<R>(requestConfig.endpoint, config);
}

export const createApiRequest = <R = undefined>(
  requestConfig: ApiRequestConfig,
  errorHandler?: CustomErrorHandler,
) =>
  async function (): Promise<ApiResult<R>> {
    try {
      let response: AxiosResponse<R> | undefined;

      switch (requestConfig.method) {
        case HttpMethods.Get:
          response = await doGet<R>(requestConfig);
          break;
        case HttpMethods.Post:
          response = await doPost<R>(requestConfig);
          break;
        case HttpMethods.Put:
          response = await doPut<R>(requestConfig);
          break;
        case HttpMethods.Patch:
          response = await doPatch<R>(requestConfig);
          break;
        case HttpMethods.Delete:
          response = await doDelete<R>(requestConfig);
          break;
      }

      return createApiSuccessResult({data: response?.data});
    } catch (error) {
      return errorHandler ? errorHandler(error) : handlerError(error);
    }
  };
