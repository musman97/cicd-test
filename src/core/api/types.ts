import {AxiosError} from 'axios';
import {Result} from '~/types';
import {HttpMethods} from './constants';

export type ApiRequestConfig<D = any> = {
  endpoint: string;
  method: HttpMethods;
  data?: D;
} & {
  withAuth: true;
  accessToken: string;
};

export interface ApiSuccessResult<D> extends Result<D, number, undefined> {
  success: true;
}

export interface ApiFailureResult<E = undefined | AxiosError | Error>
  extends Result<undefined, number, E> {
  success: false;
}

export type ApiResult<R> = ApiSuccessResult<R> | ApiFailureResult;

export type CustomErrorHandler = (error: unknown) => ApiFailureResult;

export type GeneralApiResponseData = {message?: string};

export type DataWithAccessToken<D = object> = D & {accessToken: string};
