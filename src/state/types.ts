import {
  CaseReducerActions,
  Slice as RtkSlice,
  SliceCaseReducers,
} from '@reduxjs/toolkit';
import {ApiStates} from '~/core';

type StateWithApiState = {
  [key: string]: ApiStates;
};

export type Slice<State, Name extends string> = RtkSlice<
  State,
  SliceCaseReducers<State>,
  Name
>;

export type SliceActions<State> = CaseReducerActions<SliceCaseReducers<State>>;

export type SliceStateWithApiState<State> = State & StateWithApiState;
