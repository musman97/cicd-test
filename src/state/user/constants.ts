import {TUserReducerName} from './types';

export const UserApiStateKeys = {
  LoginRequestApiState: 'loginRequestApiState',
};

export const UserReducerName: TUserReducerName = 'user';
