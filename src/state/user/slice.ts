import {PayloadAction, Reducer, createSlice} from '@reduxjs/toolkit';
import {resetApiStateExtraReducer} from '../common';
import {UserApiStateKeys, UserReducerName} from './constants';
import {UserDetails, UserState} from './types';
import {ApiStates, DataWithAccessToken} from '~/core';

const initialState = {
  splashLoading: true,
  loggedIn: false,
  details: {},
  message: '',
} as UserState;

const userSlice = createSlice({
  name: UserReducerName,
  initialState,
  reducers: {
    setSplashLoading(state, action) {
      state.splashLoading = action.payload;
    },
    setLoggedIn(state, action) {
      state.loggedIn = action.payload;
    },
    loginRequest: {
      reducer(state) {
        state[UserApiStateKeys.LoginRequestApiState] = ApiStates.Pending;
      },
      prepare(
        loginParams: DataWithAccessToken<{
          email: string;
          password: string;
        }>,
      ) {
        return {payload: loginParams};
      },
    },
    loginSuccess(state, action: PayloadAction<UserDetails>) {
      state[UserApiStateKeys.LoginRequestApiState] = ApiStates.Success;
      state.details = action.payload;
    },
    loginFailure(state, action) {
      state[UserApiStateKeys.LoginRequestApiState] = ApiStates.Failure;
      state.message = action.payload;
    },
  },
  extraReducers: resetApiStateExtraReducer,
});

export const UserActions = userSlice.actions;
export const userReducer: Reducer<UserState> = userSlice.reducer;
