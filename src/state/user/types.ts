import {SliceStateWithApiState} from '../types';

export type TUserReducerName = 'user';

export type UserDetails = {};

export type UserState = SliceStateWithApiState<{
  splashLoading: boolean;
  loggedIn: boolean;
  details: UserDetails;
  message: string;
}>;
