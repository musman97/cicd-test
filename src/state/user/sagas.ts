import {all, call, put, takeLatest} from 'redux-saga/effects';
import {ApiService} from '~/core/api/ApiService';
import {UserActions} from './slice';
import {PayloadAction} from '@reduxjs/toolkit';
import {ApiResult, DataWithAccessToken} from '~/core';
import {UserDetails} from './types';

function* loginRequest({
  payload: {email, password, accessToken},
}: PayloadAction<DataWithAccessToken<{email: string; password: string}>>) {
  const response: Awaited<ApiResult<UserDetails>> = yield call(
    ApiService.doLogin,
    {email, password, accessToken},
  );

  if (response.success && response.data) {
    put(UserActions.loginSuccess(response.data));
  } else {
    put(UserActions.loginFailure(response.message));
  }
}

export function* userWatcher() {
  yield all([takeLatest(UserActions.loginRequest, loginRequest)]);
}
