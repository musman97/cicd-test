export interface Result<D, C, E> {
  success: boolean;
  message: string;
  code: C;
  data?: D;
  cause?: E;
}
