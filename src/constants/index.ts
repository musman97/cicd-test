export enum AppEnvs {
  Staging = 'Staging',
  Prod = 'Prod',
}

export const AppEnv: AppEnvs = AppEnvs.Staging;
