export enum RouteNames {
  Splash = 'Splash',
  Login = 'Login',
}

export const defaultScreenOptions = {
  headerShown: false,
};
