import {RouteNames} from './config';
import RootNavigator from './RootNavigator';

export default RootNavigator;
export {RouteNames};
export * from './types';
