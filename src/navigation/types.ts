import {RouteNames} from './config';

export type RootStackParamList = {
  [RouteNames.Splash]: undefined;
  [RouteNames.Login]: undefined;
};
