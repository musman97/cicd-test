export const isDefined = (value: any) => value !== undefined && value !== null;

export const isNotDefined = (value: any) => !isDefined(value);

export const isObjectEmpty = (obj: object) => {
  if (isNotDefined(obj)) {
    return true;
  }

  for (const prop in obj) {
    if (obj.hasOwnProperty(prop)) {
      return false;
    }
  }

  return true;
};

export const isObjectNotEmpty = (obj: object) => !isObjectEmpty(obj);

export const isError = (error: unknown): error is Error =>
  error instanceof Error;
