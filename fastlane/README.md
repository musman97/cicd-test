fastlane documentation
----

# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```sh
xcode-select --install
```

For _fastlane_ installation instructions, see [Installing _fastlane_](https://docs.fastlane.tools/#installing-fastlane)

# Available Actions

## Android

### android debug_apk

```sh
[bundle exec] fastlane android debug_apk
```

Build debug apk and copy it to user's desktop

### android release_apk

```sh
[bundle exec] fastlane android release_apk
```

Build release apk and copy it to user's desktop

### android release_aab

```sh
[bundle exec] fastlane android release_aab
```

Build release aab and copy it to user's desktop

### android ci_release_apk

```sh
[bundle exec] fastlane android ci_release_apk
```

Build release apk CI

----


## iOS

### ios sync_appstore_profiles

```sh
[bundle exec] fastlane ios sync_appstore_profiles
```

Syncs distribution profiles and certificates

### ios sync_adhoc_profiles

```sh
[bundle exec] fastlane ios sync_adhoc_profiles
```

Syncs adhoc profiles and certificates

### ios build_ipa

```sh
[bundle exec] fastlane ios build_ipa
```

Build adhoc local ipa

### ios build_for_testflight

```sh
[bundle exec] fastlane ios build_for_testflight
```

Build ipa to be uploaded on testflight

### ios upload_ipa_testflight

```sh
[bundle exec] fastlane ios upload_ipa_testflight
```



### ios ci_build_ipa

```sh
[bundle exec] fastlane ios ci_build_ipa
```

Build adhoc ipa on circle ci

### ios ci_build_for_testflight

```sh
[bundle exec] fastlane ios ci_build_for_testflight
```

Build ipa to be uploaded on testflight on circle ci

----

This README.md is auto-generated and will be re-generated every time [_fastlane_](https://fastlane.tools) is run.

More information about _fastlane_ can be found on [fastlane.tools](https://fastlane.tools).

The documentation of _fastlane_ can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
